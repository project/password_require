
Password Require
***Important***
This module is still in pre-release phase.
Because this module aims to add extra security it should be used with caution in production sites.
Please review the code to evaluate its security.
If you find an issue please report it.


--SUMMARY--
This is module allows adding password protect to any form in Drupal. 
For node forms the password protection is control by editing the content type.

For all other forms:
1. Goto: admin/settings/password-require/set-forms
2. Click: "Start Editing Forms Protection"
3. Browse to desired form
4. Click: Add/Remove password protection
5. Submit form to change the forms status

--Permissions--
"bypass password required"
Any role with this permission will never be require to enter their password
Because user 1(uid==1) has all permission they will never have to enter their password

--Other Modules--
hook_password_require
Allows other modules to set a form password protected
If a form is password protected via this module another module cannot unprotect it.
